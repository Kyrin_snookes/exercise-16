﻿using System;

namespace exercise_16
{
    class Program
    {
        static void Main(string[] args)
        {
            

//Start the program with Clear();
Console.Clear();


 var name ="";   

System.Console.WriteLine("**__________________________________________**");
System.Console.WriteLine("**__________________________________________**");
Console.WriteLine("Hello what is your name");
name = Console.ReadLine();
System.Console.WriteLine("**__________________________________________**");
Console.WriteLine($"Hello {name}");                                 
System.Console.WriteLine("**__________________________________________**");









//End the program with blank line and instructions
Console.ResetColor();
Console.WriteLine();
Console.WriteLine("Press <Enter> to quit the program");
Console.ReadKey();

        }
    }
}
